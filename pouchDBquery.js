//PouchDB and CouchDB
var PouchDB = require('PouchDB')
var db = new PouchDB('AEMDb')

// doc1 = { _id:'0001', username: 'user@aemenersol.com', password: 'Test@123'}

// db info
// db.info().then((info) => {
//     console.log(info)
// })

// add data db
// db.put(doc1,function(err,res){
//     if(err){
//         console.log(err)
//     } else {
//         console.log("ADDED")
//     }
// })

// get data
// db.get('0001',function(err,doc){
//     if(err){
//         console.log(err)
//     } else {
//         console.log(doc)
//     }
// })

// remove data
// db.remove('id','revisionId',function(err){
//     if(err){
//         console.log(err)
//     } else {
//         console.log("DELETED")
//     }
// })

// get all data
// db.allDocs(function(err,doc){
//     if(err){
//         console.log(err)
//     } else {
//         console.log(doc.rows)
//     }
// })
