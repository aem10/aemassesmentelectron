// Pouch Db function

const addPouchDB = async (username, password, token) => {
    let loginModel = {
        _id: username,
        username: username,
        password: password,
        bearerToken: token,
    }

    db.put(loginModel, function (err, res) {
        if (err) {
            console.log(err)
        } else {
            console.log("Added data to PouchDb")
        }
    })
}

const checkPouchDB = async (username) => {
    db.get(username, function (err, doc) {
        if (err) {
            login(document.querySelector("#username").value, document.querySelector("#password").value);
        } else {
            // get token from PouchDB
            getTokenPouchDB(document.querySelector("#username").value, document.querySelector("#password").value)
        }
    })
}

const getTokenPouchDB = async (username, password) => {
    db.get(username, function (err, doc) {
        if (err) {
            isExist = false
        } else {
            if (password == doc["password"] && username == doc["username"] && doc["bearerToken"] != "") {
                localStorage.setItem('bearerToken', doc["bearerToken"])
                window.location.replace('./src/dashboard/dashboard.html');
            } else {
                console.log("Invalid Username or Password ")
            }
        }
    })
}


// Form function
const signinForm = document.querySelector("#signInForm");

document.addEventListener("DOMContentLoaded", (e) => {
    signinForm.addEventListener("submit", (e) => {
        e.preventDefault();
        checkPouchDB(document.querySelector("#username").value)
    })
})


const login = async (username, password) => {
    fetch('http://test-demo.aemenersol.com/api/account/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            username: username,
            password: password,
        }),
    }).then(res => {
        if (!res.ok) {
            res.json().then(value => {
                let userNameErrorList = value['Username']
                let passwordErrorList = value['Password']
                let error = ""

                if (userNameErrorList) {
                    userNameErrorList.forEach(element => {
                        error = error + element + "\n"
                    });
                    document.getElementById("errorUsername").innerHTML = error
                }

                error = ""

                if (passwordErrorList) {
                    passwordErrorList.forEach(element, i => {
                        error = error + element + "\n"
                    });
                    document.getElementById("errorPassword").innerHTML = error
                }

                addPouchDB(document.querySelector("#username").value, document.querySelector("#password").value, "")
                document.querySelector("#username").value = "";
                document.querySelector("#password").value = ""
            })
            // throw response;
            return;
        }

        res.json().then(token => {
            localStorage.setItem('bearerToken', token)
            addPouchDB(document.querySelector("#username").value, document.querySelector("#password").value, token)
            window.location.replace('./src/dashboard/dashboard.html');
        })
    })
}

