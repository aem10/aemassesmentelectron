const signOutForm = document.querySelector("#signOutForm");

document.addEventListener("DOMContentLoaded", (e) => {
    signOutForm.addEventListener("submit", (e) => {
        e.preventDefault();
        localStorage.clear()
        console.log("click")
        window.location.replace('../../index.html');
    })
})